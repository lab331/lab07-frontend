import { Component, OnInit } from '@angular/core';
import Student from '../../entity/student';
import { StudentService } from '../../service/student-service';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-students-add',
  templateUrl: './students.add.component.html',
  styleUrls: ['./students.add.component.css']
})
export class StudentsAddComponent implements OnInit {


  model: Student = new Student();
  fb: FormBuilder = new FormBuilder;
  sf: FormGroup;
  validation_message = {
    'studentId': [
      {type: 'required', message: 'student id is required'},
      {type: 'maxlength', message: 'student id is too long'},
      {type: 'minlength', message: 'student id is too short'},
      {type: 'pattern', message: 'please enter number'}
    ],
    'name': [
      {type: 'required', message: 'student name is required'}
    ],
    'surname': [
      {type: 'required', message: 'student surname is required'}
    ],
    'penAmount': [
      {type: 'required', message: 'student penAmount is required'},
      {type: 'pattern', message: 'please enter number'}
    ],
    'image': [],
    'description': []
  };

  constructor(private studentService: StudentService, private router: Router) {

  }
  ngOnInit(): void {
    this.sf = this.fb.group({
      // tslint:disable-next-line:max-line-length
      studentId: [null, Validators.compose([Validators.required, Validators.maxLength(10), Validators.minLength(10), Validators.pattern('[0-9]+')])],
      name: [null, Validators.required],
      surname: [null, Validators.required],
      penAmount: [null, Validators.compose([Validators.required, Validators.pattern('[0-9]+')])],
      image: [null],
      description: [null]
    });
  }
  upQuantity(student: Student) {
    student.penAmount++;
  }

  downQuantity(student: Student) {
    if (student.penAmount > 0) {
      student.penAmount--;
    }
  }
  onSubmit() {
    this.model = this.sf.value;
    this.studentService.saveStudent(this.model)
      .subscribe((student) => {
        this.router.navigate(['/detail', student.id]);
      }, (error) => {
        alert('could not save value');
      });
  }
  // TODO: Remove this when we're done
  get diagnostic() { return JSON.stringify(this.model); }
}
